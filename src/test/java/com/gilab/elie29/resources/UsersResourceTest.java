package com.gilab.elie29.resources;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.greaterThan;

@QuarkusTest
public class UsersResourceTest {

    @Test
    public void testHelloEndpoint() {
        given()
            .when().get("/api/users")
            .then()
            .statusCode(OK.getStatusCode())
            .body("$.size()", is(greaterThan(1)),
                "[0].id", is(notNullValue())
            );
    }

}