package com.gilab.elie29.services;

import com.gilab.elie29.dto.User;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Set;

@RegisterRestClient
@Singleton
@Path("/users")
public interface UsersService {

    @GET
    @Produces("application/json")
    Uni<Set<User>> getUsers();
}
