package com.gilab.elie29.dto;

import lombok.Data;

@Data
public class User {
    private Long id;
    private String name;
    private String username;
    private String email;
    private Company company;
}
