package com.gilab.elie29.dto;

import lombok.Data;

@Data
public class Company {
    private String name;
    private String catchPhrase;
}
