package com.gilab.elie29.resources;

import com.gilab.elie29.dto.User;
import com.gilab.elie29.services.UsersService;
import io.smallrye.mutiny.Uni;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/api/users")
@Slf4j
public class UsersResource {

    @Inject
    @RestClient
    UsersService usersService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Set<User>> getUsers() {
        log.info("getUsers called");
        return usersService.getUsers();
    }
}